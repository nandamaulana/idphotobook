const mongoose = require('mongoose');
const { ObjectId } = mongoose.Schema;

const Schema = mongoose.Schema;
const ProductSchema = new Schema({
  name: {
    type: String,
    required: true
  },
  description: String,
  image: [
    {
      type: ObjectId,
      ref: "image"
    }
  ],
  user: {
    type: ObjectId,
    ref: "user"
  }
});
module.exports = mongoose.model('product', ProductSchema);

