var express = require('express');
var bodyParser = require('body-parser');
var Product = require('./../models/product');
var User = require('./../models/user');
var router = express.Router();

router.use(bodyParser.urlencoded({ extended: true }));

router.use(bodyParser.json());
router.use(function (req, res, next) {
    console.log("request");
    next();
});

router.get('/', function (req, res) {
    res.sendFile('../views/index.html');
});

router.route('/products')
    .post(async function (req, res) {
        var products = new Product();
        const {
            name,
            description,
            image,
            idUser
        } = req.body;

        const user = await User.findOne({ _id: idUser });

        if (!user) {
            return res.status(404).json({ message: "User not found" });
        }
        if (image.length <= 10) {
            return res.status(404).json({ message: "Image length cannot below 10" });
        }

        products.name = name;
        products.user = user.id;
        products.description = description;
        products.image = image;


        //response
        products.save(function (error) {
            if (error)
                res.status(500).send('Failed to register new product. ERROR: ' + error);
            res.json({ message: "product successfully registered" });
        });
    })
    .get(function (req, res) {
        Product.find(function (error, products) {
            if (error)
                res.status(500).send("Failed to show products. ERROR: " + error);
            res.json(products);
        });
    });

/* [2nd-ROUTE] - '/products/:product_id' - used for ((2)GET, (3)PUT AND (4)DELETE) by id */
router.route('/products/:product_id')
    /* ----------------------------------------------------------------------------------------
          (2) READ ONE PRODUCT(by id) - (GET) - http://localhost:3000/api/products/product_id
       ----------------------------------------------------------------------------------------*/
    .get(function (req, res) {
        Product.findById(req.params.product_id, function (error, product) {
            if (error)
                res.status(500).send('error: ' + error);
            res.json(product);
        })
    })
    /* ---------------------------------------------------------------------------------------
          (3) UPDATE PRODUCT(by id) - (PUT) - http://localhost:3000/api/products/product_id
       ---------------------------------------------------------------------------------------*/
    .put(function (req, res) {
        //serch a product by id - with id in req
        Product.findById(req.params.product_id, function (error, product) {
            if (error)
                res.send('error: ' + error);
            //update attributes of the product with req fields
            product.name = req.body.name;
            product.amount = req.body.amount;
            product.description = req.body.description;
            //save
            product.save(function (error) {
                if (error)
                    res.status(500).send('Failed to update product. ERROR: ' + error);
                res.json({ message: 'Product update successful!' });
            });
        });
    })
    /* ---------------------------------------------------------------------------------------
         (4) DELETE PRODUCT(by id) - (DELETE) - http://localhost:3000/api/products/product_id
       ---------------------------------------------------------------------------------------*/
    .delete(function (req, res) {
        Product.remove({
            _id: req.params.product_id
        }, function (error) {
            if (error)
                res.status(500).send('Unable to find product by id. Failed to remove. ERROR: ' + error);
            res.json({ message: 'Product deleted successful!' });
        });
    });

//export
module.exports = router;